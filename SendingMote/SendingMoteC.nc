/*
 * Copyright (c) 2008 Dimas Abreu Dutra
 * All rights reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 * - Neither the name of the Stanford University nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL DIMAS ABREU
 * DUTRA OR HIS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @author Dimas Abreu Dutra
 */

#include "ApplicationDefinitions.h"
#include "RssiReceiverMessages.h"

module SendingMoteC {
  uses interface Boot;
  uses interface Timer<TMilli> as SendTimer;
  uses interface Packet;
  uses interface AMSend as RssiMsgSend;
  uses interface SplitControl as RadioControl;
  uses interface Receive;
} implementation {
  message_t msg;
  RssiMsg* rssimsg;


  event void Boot.booted(){
    call RadioControl.start();
    rssimsg=(RssiMsg*)(call Packet.getPayload(&msg, sizeof(RssiMsg)));
    rssimsg->node_id=TOS_NODE_ID;
  }

  event void RadioControl.startDone(error_t result){
	if(TOS_NODE_ID==0) //se è il nodo 0 fa partire il timer
    		call SendTimer.startPeriodic(SEND_INTERVAL_MS);
  }

  event void RadioControl.stopDone(error_t result){}

  event message_t* Receive.receive(message_t* msgR, void* payload, uint8_t len){
    if (len == sizeof(RssiMsg) && (TOS_NODE_ID==1 || TOS_NODE_ID==2)) {	
//se il messaggio e" RSSI e io sono o il nodo 1 o il nodo 2 allora ricevo il messaggio se no lo ignoro
      RssiMsg* recpkt = (RssiMsg*)payload;
//se l id del messaggio ricevuto [ quello del mio predecessore allora mando il mio messaggio
	if(recpkt->node_id == TOS_NODE_ID-1)
      call RssiMsgSend.send(AM_BROADCAST_ADDR, &msg, sizeof(RssiMsg));  
    }
    return msgR;
  }


  event void SendTimer.fired(){
    call RssiMsgSend.send(AM_BROADCAST_ADDR, &msg, sizeof(RssiMsg));    
  }

  event void RssiMsgSend.sendDone(message_t *m, error_t error){}
}
