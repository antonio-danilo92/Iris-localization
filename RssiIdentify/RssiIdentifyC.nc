#include "ApplicationDefinitions.h"
#include "RssiReceiverMessages.h"  
#include "RssiIdentifyMessages.h"  

#include <math.h>
module RssiIdentifyC {
 // uses interface Intercept as RssiMsgIntercept;
  uses interface Timer<TMilli> as SendTimer;
  uses interface Packet;
  uses interface Leds;
  uses interface Boot;
  uses interface Receive;
  uses interface SplitControl as RadioControl;
  uses interface AMSend as PosMsgSender;
#ifdef __CC2420_H__
  uses interface CC2420Packet;
#elif defined(TDA5250_MESSAGE_H)
  uses interface Tda5250Packet;    
#else
  uses interface PacketField<uint8_t> as PacketRSSI;
#endif 
} implementation {
 
  void trilateration(double x1, double y1, double r1,double x2, double y2, double r2,double x3, double y3,double r3,double *xnm,double *ynm);
  uint16_t getRssi(message_t *msg);
  double sHypot(double,double);
void baricentro(double x1, double y1, 
		double x2, double y2, 
		double x3, double y3,
		double *barX, double *barY);
void puntoInterno(double x1i, double y1i, 
		  double x2i, double y2i, 
		  double cx, double cy, double r,
		  double *pix,double *piy);
void estendiRaggio(double *r1,double *r2,double *r3,double);
int verificaIntersezione(double x1,double y1,double r1,
			 double xi1,double yi1,
			 double xi2,double yi2,
			 double *px,double *py);
int controllaIntersezione(double x1,double y1,double r1,
			  double x2,double y2,double r2, double,double,double);
void trovaPunti(double x1, double y1, double r1, double x2, double y2, double r2,
		double *x1i, double *y1i, double *x2i, double *y2i);


  uint8_t rssi1=0;
  uint8_t rssi2=0;
  uint8_t rssi3=0;
  
  const double epsilon = 0.001; 
  const double epsilonTolleranza = 0.01;
  message_t smsg;
  PosMsg *posmsg;
  RssiMsg *rssiMsg; 
  uint8_t curr;

  event void Boot.booted(){
	call RadioControl.start();
}
  event message_t* Receive.receive(message_t *msg,
				      void *payload,
				      uint8_t len) {

   rssiMsg = (RssiMsg*) payload;
    curr =rssiMsg->node_id;
    if(curr==0){
        call Leds.led0Toggle();
	rssi1=getRssi(msg);}
    else if(curr==1){
         call Leds.led1Toggle();
	rssi2=getRssi(msg);
}
    else if(curr==2){
        call Leds.led2Toggle();
	rssi3=getRssi(msg);
    }
    return msg;
  }

  event void RadioControl.startDone(error_t result){
    call SendTimer.startPeriodic(SEND_INTERVAL_MS);
  }

 event void SendTimer.fired(){
    double x;
    double d1,d2,d3;
    double y;
    posmsg=(PosMsg*)(call Packet.getPayload(&smsg, sizeof(PosMsg)));
    posmsg->rssi1=rssi1;
    posmsg->rssi2=rssi2;
    posmsg->rssi3=rssi3;

    rssi1-=91;
    rssi2-=91;
    rssi3-=91;
    d1=(0.0003*rssi1*rssi1)-(0.0003*rssi1)-0.2667;
    d2=(0.0003*rssi2*rssi2)-(0.0003*rssi2)-0.2667;
    d3=(0.0003*rssi3*rssi3)-(0.0003*rssi3)-0.2667;
    trilateration(0.0,0.0, d1, 2.0, 0.0, d2, 1.0, 1.0, d3,&x,&y);
    posmsg->x=((uint16_t)(x*1000));
    posmsg->y=((uint16_t)(y*1000));
    call PosMsgSender.send(4, &smsg, sizeof(PosMsg));    
  }

  event void PosMsgSender.sendDone(message_t *m, error_t error){}
  event void RadioControl.stopDone(error_t result){}

  uint16_t getRssi(message_t *msg){
    if(call PacketRSSI.isSet(msg))
      return (uint16_t) call PacketRSSI.get(msg);
    else
      return 0xFFFF;
  }
int controllaIntersezione(double x1,double y1,double r1,
			  double x2,double y2,double r2,
			  double x3,double y3,double r3){

	double dx,dy,d,dmax=0;
	//controllo se c1 interseca con c2
	dx = x2 - x1;
	dy = y2 - y1;
	d=sqrt((dy*dy) + (dx*dx))-r1-r2;
	if (d>0)
		dmax=d;
	
	dx = x3 - x1;
	dy = y3 - y1;
	d=sqrt((dy*dy) + (dx*dx))-r1-r3;
	if (d>0 && d>dmax)
		dmax=d;

	dx = x2 - x3;
	dy = y2 - y3;
	d=sqrt((dy*dy) + (dx*dx))-r2-r3;
	if (d>0 && d>dmax)
		dmax=d;

	if (dmax>0)
		return dmax;
	else	
		return 0;
}

void trovaPunti(double x1, double y1, double r1, double x2, double y2, double r2,
		double *x1i, double *y1i, double *x2i, double *y2i){
	/*Determine the distance from point 0 to point 2. */
	double dx,dy,xi,yi,rx,ry,h,a,d; 
	
	dx = x2 - x1;
  	dy = y2 - y1;

  	/* Determine the straight-line distance between the centers. */
	d=sqrt((dy*dy) + (dx*dx));
 	
	a = ((r1*r1) - (r2*r2) + (d*d)) / (2.0 * d) ;

	/* Determine the coordinates of point 2. */	
  	xi = x1 + (dx * a/d);
 	yi = y1 + (dy * a/d);

  	/* Determine the distance from point 2 to either of the
   	* intersection points.
   	*/
  	h = sqrt((r1*r1) - (a*a));

  	/* Now determine the offsets of the intersection points from
   	* point 2.
   	*/
  	rx = -dy * (h/d);
  	ry = dx * (h/d);

  	/* Determine the absolute intersection points. */
 	*x1i = xi + rx;
  	*x2i = xi - rx;
  	*y1i = yi + ry;
  	*y2i = yi - ry;
}

void estendiRaggio(double *r1,double *r2,double *r3,double extendR){
	*r1+=extendR;
	*r2+=extendR;
	*r3+=extendR;
}
int verificaIntersezione(double x1,double y1,double r1,
			 double xi1,double yi1,
			 double xi2,double yi2,
			 double *px,double *py){ //verifico se la terza cinconferenza passa per uno dei due punti	
	
	double dx,dy,d1,d2;	
	dx = xi1 - x1;
   	dy = yi1 - y1;
	d1=sqrt((dy*dy) + (dx*dx));

    	dx = xi2 - x1;	
    	dy = yi2 - y1;
    	d2=sqrt((dy*dy) + (dx*dx));
	
    	if(fabs(d1 - r1) < epsilonTolleranza) {
		*px=xi1;
		*py=yi1;
		 return 1;       	
	} else if(fabs(d2 - r1) < epsilonTolleranza) {
			*px=xi2;
			*py=yi2;
			return 1;
	} else return 0;
}    	
void puntoInterno(double x1i, double y1i, 
		  double x2i, double y2i, 
		  double cx, double cy, double r,
		  double *pix,double *piy){
	
	double dx,dy,d1,d2; 
	
	dx = cx - x1i;
  	dy = cy - y1i;
	d1=sqrt((dy*dy) + (dx*dx));

	dx = cx - x2i;
  	dy = cy - y2i;
	d2=sqrt((dy*dy) + (dx*dx));

	if (d1<d2){
		*pix=x1i;
		*piy=y1i;
	} else {
		*pix=x2i;
		*piy=y2i;
	}
}
void baricentro(double x1, double y1, 
		double x2, double y2, 
		double x3, double y3,
		double *barX, double *barY){ // Coordinate dei punti di intersezione 
	
	*barX = (x1+x2+x3)/3; //ottengo la coordinata x 
	*barY = (y1+y2+y3)/3; //coordinate y
}


void trilateration(double x1, double y1, double r1,
              double x2, double y2, double r2,
              double x3, double y3, double r3,
		double *xnm,double *ynm){
	int flag;
	double xint1,xint2,yint1,yint2;
	double px0,py0,px1,py1,px2,py2;
	double px3,py3; //punto di intersezione delle 3 circonferenze oppure baricentro triangolo area interna;
	
	int extendR=controllaIntersezione(x1,y1,r1,x2,y2,r2,x3,y3,r3)/2;
	if (extendR>0)		
		estendiRaggio(&r1,&r2,&r3,extendR);

	trovaPunti(x1,y1,r1,x2,y2,r2,&xint1,&yint1,&xint2,&yint2); //trovo l'intersezione tra prima e seconda circonferenza
	flag=verificaIntersezione(x3,y3,r3,xint1,yint1,xint2,yint2,&px3,&py3);
	if (flag!=1){
		puntoInterno(xint1,yint1,xint2,yint2,x3,y3,r3,&px0,&py0); // cerco il primo punto interno
		trovaPunti(x1,y1,r1,x3,y3,r3,&xint1,&yint1,&xint2,&yint2);
		puntoInterno(xint1,yint1,xint2,yint2,x2,y2,r2,&px1,&py1); // cerco il secondo punto interno
		trovaPunti(x2,y2,r2,x3,y3,r3,&xint1,&yint1,&xint2,&yint2);
		puntoInterno(xint1,yint1,xint2,yint2,x1,y1,r1,&px2,&py2); // cerco il terzo punto interno
		baricentro(px0,py0,px1,py1,px2,py2,&px3,&py3);
	}
	*xnm=px3;
	*ynm=py3;
}

double sHypot(double pointX,double pointY){
	return (sqrt(pointX*pointX+pointY*pointY));
}

}
